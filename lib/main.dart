import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:noamir/screens/wrapper.dart';
import 'package:noamir/services/auth.dart';
import 'package:provider/provider.dart';

void main() => runApp(Noamir());


class Noamir extends StatelessWidget { 
  @override
  Widget build(BuildContext context) {
    return StreamProvider<FirebaseUser>.value(
      value:  AuthenticationService().user,
          child: MaterialApp(
        home: Wrapper(),
      ),
    );
  }
}

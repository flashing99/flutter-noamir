import 'package:flutter/material.dart';
import 'package:noamir/widgets/signin_card.dart';
import 'package:noamir/widgets/signup_card.dart';

enum AuthMode { LOGIN, SINGUP }

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool _showSignIn = true;
  void toggleView() {
    setState(() => _showSignIn = !_showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            LowerHalf(),
            UpperHalf(),
            PageTitle(),
            _showSignIn
                ? SignInCard(toggleView: toggleView)
                : SignUpCard(toggleView: toggleView)
          ],
        ),
      ),
    );
  }
}

class PageTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.only(top: 30.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "NOAMIR",
              style: TextStyle(
                fontSize: 48,
                color: Colors.white,
                fontWeight: FontWeight.w400,
                letterSpacing: 4.0,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class UpperHalf extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: screenHeight / 2,
      child: Image.asset(
        'assets/city.jpg',
        fit: BoxFit.cover,
      ),
    );
  }
}

class LowerHalf extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: screenHeight / 2,
        color: Color(0xFFECF0F3),
      ),
    );
  }
}

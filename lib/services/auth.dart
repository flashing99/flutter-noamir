import 'package:firebase_auth/firebase_auth.dart';

class AuthenticationService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Stream<FirebaseUser> get user {
    return _auth.onAuthStateChanged;
  }

  Future<FirebaseUser> signIn() {
    return null;
  }

  Future<FirebaseUser> signUp() {
    return null;
  }

  Future<void> logOut() async {
    await _auth.signOut();
  }
}
